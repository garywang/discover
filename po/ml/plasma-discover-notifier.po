# Malayalam translations for discover package.
# Copyright (C) 2019 This file is copyright:
# This file is distributed under the same license as the discover package.
# Automatically generated, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: discover\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-04-03 02:09+0000\n"
"PO-Revision-Date: 2021-09-26 16:02+0000\n"
"Last-Translator: Subin Siby <subins2000@gmail.com>\n"
"Language-Team: Malayalam <https://kde.smc.org.in/projects/discover/plasma-"
"discover-notifier/ml/>\n"
"Language: ml\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Weblate 4.7.2\n"

#: notifier/DiscoverNotifier.cpp:154
#, kde-format
msgid "View Updates"
msgstr "അപ്ഡേറ്റുകൾ കാണുക"

#: notifier/DiscoverNotifier.cpp:271
#, kde-format
msgid "Security updates available"
msgstr "സുരക്ഷാ അപ്ഡേറ്റുകൾ ലഭ്യമാണ്"

#: notifier/DiscoverNotifier.cpp:273
#, kde-format
msgid "Updates available"
msgstr "അപ്ഡേറ്റുകൾ ലഭ്യമാണ്"

#: notifier/DiscoverNotifier.cpp:275
#, kde-format
msgid "System up to date"
msgstr "സിസ്റ്റം കാലികമാണ്"

#: notifier/DiscoverNotifier.cpp:277
#, kde-format
msgid "Computer needs to restart"
msgstr "കമ്പ്യൂട്ടർ പുനരാരംഭിക്കേണ്ടതുണ്ട്"

#: notifier/DiscoverNotifier.cpp:279
#, kde-format
msgid "Offline"
msgstr "ഓഫ്‌ലൈൻ"

#: notifier/DiscoverNotifier.cpp:281
#, kde-format
msgid "Applying unattended updates…"
msgstr ""

#: notifier/DiscoverNotifier.cpp:315
#, kde-format
msgid "Restart is required"
msgstr "പുനരാരംഭിക്കേണ്ടത് ആവശ്യമാണ്"

#: notifier/DiscoverNotifier.cpp:316
#, kde-format
msgid "The system needs to be restarted for the updates to take effect."
msgstr "അപ്ഡേറ്റുകൾ പ്രാബല്യത്തിൽ വരുന്നതിന് സിസ്റ്റം പുനരാരംഭിക്കേണ്ടതുണ്ട്."

#: notifier/DiscoverNotifier.cpp:322
#, kde-format
msgctxt "@action:button"
msgid "Restart"
msgstr "പുനരാരംഭിക്കുക"

#: notifier/DiscoverNotifier.cpp:342
#, kde-format
msgctxt "@action:button"
msgid "Upgrade"
msgstr "അപ്ഗ്രേഡ്"

#: notifier/DiscoverNotifier.cpp:343
#, kde-format
msgid "Upgrade available"
msgstr "അപ്ഗ്രേഡ് ലഭ്യമാണ്"

#: notifier/DiscoverNotifier.cpp:344
#, kde-format
msgid "New version: %1"
msgstr "പുതിയ പതിപ്പ്: %1"

#: notifier/main.cpp:39
#, kde-format
msgid "Discover Notifier"
msgstr ""

#: notifier/main.cpp:41
#, kde-format
msgid "System update status notifier"
msgstr ""

#: notifier/main.cpp:43
#, fuzzy, kde-format
#| msgid "© 2010-2021 Plasma Development Team"
msgid "© 2010-2022 Plasma Development Team"
msgstr "© 2010-2021 പ്ലാസ്മ വികസന ടീം"

#: notifier/main.cpp:47
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "നിസരി കെ കെ"

#: notifier/main.cpp:47
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "nisarikoolath@gmail.com"

#: notifier/main.cpp:52
#, kde-format
msgid "Replace an existing instance"
msgstr ""

#: notifier/main.cpp:54
#, kde-format
msgid "Do not show the notifier"
msgstr ""

#: notifier/main.cpp:54
#, kde-format
msgid "hidden"
msgstr "മറച്ചിരിക്കുന്നു"

#: notifier/NotifierItem.cpp:35 notifier/NotifierItem.cpp:36
#, kde-format
msgid "Updates"
msgstr "അപ്ഡേറ്റുകൾ"

#: notifier/NotifierItem.cpp:50
#, kde-format
msgid "Open Discover…"
msgstr "ഡിസ്കവർ തുറക്കുക…"

#: notifier/NotifierItem.cpp:55
#, kde-format
msgid "See Updates…"
msgstr "അപ്ഡേറ്റുകൾ കാണുക…"

#: notifier/NotifierItem.cpp:60
#, kde-format
msgid "Refresh…"
msgstr "പുതുക്കുക…"

#: notifier/NotifierItem.cpp:64
#, kde-format
msgid "Restart to apply installed updates"
msgstr "ഇൻസ്റ്റാൾ ചെയ്ത അപ്ഡേറ്റുകൾ പ്രയോഗിക്കാൻ പുനരാരംഭിക്കുക"

#: notifier/NotifierItem.cpp:65
#, kde-format
msgid "Click to restart the device"
msgstr "ഉപകരണം പുനരാരംഭിക്കാൻ ക്ലിക്കുചെയ്യുക"
